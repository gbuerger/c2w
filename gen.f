
      INCLUDE 'c2w.par'
      PARAMETER (nd0=31)
      INTEGER MON(12), iy, im, id
      REAL tol, x(n), xm(n), W(n,nd0), w1(n)
      CHARACTER lfmt*80, fmt*80, date*7, c*1
      LOGICAL is_leap
      DATA lfmt/'(I4,1H-,I2.2,6(F8.1))'/
      DATA fmt/'(I4,1H-,I2.2,1H-,I2.2,6(F8.1))'/
      DATA tol/5e-3/
      DATA MON/31,28,31,30,31,30,31,31,30,31,30,31/

      open (7, FILE='c2w.log')

      read(5,*)
      read(5,*)
      read(5,*)
      read(5,*,err=9999,end=8000) (xm(i),i=1,n)
      read(5,*)
      read(5,*)

 100  continue
      read(5,*,err=9999,end=8000) date, (x(i),i=1,n)
      read(date,'(I4,A1,I2)') iy, c, im

      nd = MON(im)
      if (is_leap(iy) .and. im .eq. 2) then
         nd = 29
      end if
      
      iseed = im
      call C2W (xm,x,nd,iseed,tol,W)

      do is=1,n
         w1(is) = 0
      enddo

      do id=1,nd
         do is=1,n
            w1(is) = w1(is) + W(is,id)
         enddo
      enddo

      write(7,lfmt) iy, im, (x(j),j=1,n)
      write(7,lfmt) iy, im, (w1(j)/nd,j=1,n)
      write(7,'(/)')

      do id = 1,nd
         write(6,fmt) iy, im, id, (W(j,id),j=1,n)
      end do

      goto 100

      goto 8000
 9000 stop 'EOF !'
 9999 stop 'ERR !'
 8000 continue
      close(7)
      end program
