      REAL*4 FUNCTION YFCT (xx,pp,ifct)
      INCLUDE 'c2w.par'
      IMPLICIT REAL*4 (A-H,O-Z)
      REAL*4 p(2)
      COMMON /fct/ P0(n,4) 

      p(1) = P0(ifct,1) + pp*P0(ifct,3)
      p(2) = P0(ifct,2) + pp*P0(ifct,4)

      if (ifct .eq. 1) then
         yfct = (xx - p(1)) / p(2)
      elseif (ifct .eq. 2) then
         yfct = (xx - p(1)) / p(2)
      elseif (ifct .eq. 3) then
         p3 = 0.2525
         yfct = (xx**p(2) - p3) / p(1)
      elseif (ifct .eq. 4) then
         yfct = (xx - p(1)) / p(2)
      elseif (ifct .eq. 5) then
         yfct = (xx - p(1)) / p(2)
      elseif (ifct .eq. 6) then
         yfct = (xx - p(1)) / p(2)
      endif
      return
      end

      REAL*4 FUNCTION XFCT (yy,pp,ifct)
      INCLUDE 'c2w.par'
      IMPLICIT REAL*4 (A-H,O-Z)
      REAL*4 p(2)
      COMMON /fct/ P0(n,4) 

      p(1) = P0(ifct,1) + pp*P0(ifct,3)
      p(2) = P0(ifct,2) + pp*P0(ifct,4)

      if (ifct .eq. 1) then
         xfct = p(1) + p(2)*yy
      elseif (ifct .eq. 2) then
         xfct = p(1) + p(2)*yy
         xfct = MAX(0.0,xfct)
      elseif (ifct .eq. 3) then
         p3 = 0.2525
         if (p3 + p(1)*yy > 0) then
            xfct = (p3 + p(1)*yy)**(1/p(2))
         else
            xfct = 0
         endif
         xfct = MAX(0.0,xfct)
      elseif (ifct .eq. 4) then
         xfct = p(1) + p(2)*yy
         xfct = MAX(0.0,xfct)
         xfct = MIN(100.0,xfct)
      elseif (ifct .eq. 5) then
         xfct = p(1) + p(2)*yy
         xfct = MAX(0.0,xfct)
         xfct = MIN(8.0,xfct)
      elseif (ifct .eq. 6) then
         xfct = p(1) + p(2)*yy
         xfct = MAX(0.0,xfct)
         xfct = MIN(100.0,xfct)
      endif

      return
      end
