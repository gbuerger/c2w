CMPL =	gfortran -O
LIB  =	-L. -lc2w
LD = ar ruU
#LD = gfortran -fPIC
#LDFLAGS = -shared 
OBJS =	surev.o fpsuev.o fct.o fpbisp.o fpbspl.o genmn.o gennor.o sgemv.o \
	getcgn.o ignlgi.o initgn.o inp.o inrgcm.o mltmod.o pa_m.o qrgnin.o \
	ranf.o root.o sdot.o setall.o setgmn.o snorm.o splev.o spofa.o is_leap.o

gen:	libc2w.a gen.f
	$(CMPL) -o gen gen.f $(LIB)

#libc2w.so:	$(OBJS) c2w.o
#	$(LD) $(LDFLAGS) $(OBJS) c2w.o -o libc2w.so

libc2w.a:	$(OBJS) c2w.o
	$(LD) $(LDFLAGS) libc2w.a $(OBJS) c2w.o

clean:
	rm -f *.o *.so *.a gen

c2w.o:	bsp.par c2w.par c2w.f
	$(CMPL) -c c2w.f

.f.o:	c2w.par
	$(CMPL) -c $(*).f
