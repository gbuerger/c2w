C%
      SUBROUTINE INP(A,X,Y,Z,U,V,W,L,R)
      REAL A,B,C,L,P,Q,R,S,T,U,V,W,X,Y,Z
      S = Z - X
      T = (Y-X)/S
      A = (U-V)/T + (W-V)/(1.-T)
      IF ( A .EQ. 0. ) GOTO 40
      B = .5*(W-U)/A - .5
      C = U/A
      T = SQRT(ABS(C))
      IF ( ABS(B) .LT. SIGN(T,C) ) GOTO 60
      T = AMAX1(T,ABS(B))
      IF ( T .EQ. 0. ) GOTO 50
      Q = 1./T
      P = SQRT((Q*B)**2 - Q*C*Q)
      P = T*P
      IF ( ABS(P+B) .GT. ABS(P-B) ) GOTO 10
      Q = P - B
      GOTO 20
10    Q = -(B+P)
20    P = C/Q
      Q = X + S*Q
      P = X + S*P
      IF ( Q .LT. L ) GOTO 30
      IF ( Q .GT. R ) GOTO 30
      A = Q
      RETURN
30    A = P
      RETURN
40    IF ( U .EQ. W ) GOTO 50
      A = X + S*U/(U-W)
      RETURN
50    A = L
      RETURN
60    A = X - S*B
      RETURN
      END
