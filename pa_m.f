
      REAL FUNCTION PA_M (a,wu)

      INCLUDE 'c2w.par'
      PARAMETER (lw=4*2, liw=2)
      REAL wu(*)
      REAL w1(lw)
      INTEGER iw(liw)
      COMMON /spl/ nx, ny, nc, tx(nt), ty(nt), c2(nc2)

      p = wu(1)
      x = wu(2)

      idim = 1
      call SUREV (idim,tx,nx,ty,ny,c2,p,1,a,1,ww,1,w1,lw,iw,liw,ier) 

      pa_m = x - ww

      return
      end
