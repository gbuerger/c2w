
      SUBROUTINE C2W (xm,x,m,iseed,tol,W)

C_______________________________________________________________________________
C
C  The purpose of C2W is to disaggregate long-term climatological means
C  and, optionally, specific aggregation units such as monthly means, of
c  the following core meteorological quantities to daily values:
C  
C  
C  average temperature (tav [C]),
C  temperature range (trg [C]),
C  precipitation (prc [mm/day]),
C  releative humidity (rhu [%]),
C  cloudiness (cld [1/8]),
C  relative sunshine duration (rsd [%])
C
C
C  The disaggregation is understood in a stochastic sense, so that the
C  *expected* re-aggregation of the C2W output equals the input.  By paying
C  computation time, these C2W stochastics can be tricked into almost
C  exactly reproducing the input, as follows: One can define a tolerance,
C  given as the relative error between input x and aggregated output, so
C  that the routine only returns values within this tolerance. Note that
C  the value of tol and the C2W computation time are reciprocally
C  related. A value of tol = 0.005 gives acceptable results; zero tolerance
C  tells C2W to ignore this condition.
C  
C  C2W has a builtin 'memory' in the sense that the final record of a C2W
C  output is present in the next call. This enables the generation of
C  weather sequences across several aggregation units (months).
C  
C  The method is based on the following two steps: Two special functions
C  are used to translate the two input arrays xm and x to the parameters of
C  a probit normalization. The weather-generating process y is done
C  exclusively in the normalized domain, creating a 6-dimensional
C  autoregressive process
C
C                         y(t+1) = S*y(t) + e(t)
C
C  
C  with appropriate system matrix S and white noise process e. The
C  anomalies are then simply added
C
C                            y(t) = y(t) + a
C
C  and a realistic daily weather record is produced via the inverse
C  probit.
C
C
C  The above mentioned functions which translate xm and x to the probit
C  parameters, and the anomaly a, are found by using Monte Carlo
C  techniques.
C
C  All other model parameters are calibrated using European stations, so
C  that applications of C2W outside Europe should be taken with caution.
C
C  Reference: G. B�rger, 1997. On the disaggregation of climatological
C  means and anomalies. Clim Res 183-94.
C
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
C  The calling statement is
C
C        call C2W (xm,x,m,iseed,tol,W)
C
C
C        with the following declarations:
C
C         xm (input):     real array of dimension at least 6; xm
C                         contains the long-term climatological means of
C                         daily values of tav, trg, prc, rhu, cld, and rsd.
C
C          x (input):     real array of dimension at least 6; x contains
C                         some aggregation of the same variables, e.g.
C                         monthly means. If x is not available,
C                         just let x = xm.
C
C          m (input):     integer, contains the number of days to be generated.
C
C      iseed (input):     integer, used to initialize the random number generator.
C
C        tol (input):     real, tolerance of relative error in re-aggregated
C                         output.
C
C         W (output):     two-dimensional real array with first dimension
C                         exactly 6 and second dimension at least m;
C                         (W(i,j),i=1,6) contains the j-th weather record
C                         for the variables in xm. The realizations W are
C                         distributed with mean x.
C_______________________________________________________________________________

      INCLUDE 'c2w.par'
      EXTERNAL PA_M
      REAL xm(*), x(*), a(n), p(n), W(n,m), S(n,n), Q(n,n), wn(n),
     *     parm(n*(n+3)/2+1), wu(n), ym(n), e(n), wu1(n), wu2(n), wm(n)

      INTEGER X_k, X_ns, X_nx, X_ny, X_nc
      DIMENSION X_k(n), X_ns(n), X_nx(n), X_ny(n), X_nc(n)
      DIMENSION X_t(nt,n), X_tx(nt,n), X_ty(nt,n), X_c1(nt,n)
      INTEGER w_k, w_ns
      DIMENSION w_t(nt), w_c1(nt)
      DIMENSION X_c2(nc2,n)

      INTEGER ind(n)
      COMMON /spl/ nx, ny, nc, tx(nt), ty(nt), c2(nc2)
      COMMON /fct/ P0(n,4)
      DATA liu/0/, logi/0/
C      SAVE log, wu1
      SAVE

      iter = 0
 1000 iter = iter + 1
      if (iter .gt. iter0 .and. logi .eq. 0) then
         write(6,*)
     *        'warning from C2W: tolerance (tol) probably too small'
         logi = 1
      endif

      it = 1

      if (log .eq. 0) then

         iu = 1
         open(iu,file='bsp.par',form='formatted')

         do k=1,n
            read(iu,*) j1, X_k(k), j3, X_ns(k)
            read(iu,*) (X_t(j,k),j=1,X_ns(k))
            read(iu,*) j1, X_k(k), j3, X_ns(k)
            read(iu,*) (X_c1(j,k),j=1,X_ns(k))
            read(iu,*) X_nx(k), (X_tx(j,k),j=1,X_nx(k))
            read(iu,*) X_ny(k), (X_ty(j,k),j=1,X_ny(k))
            read(iu,*) X_nc(k), (X_c2(j,k),j=1,X_nc(k))
         enddo
         do k=1,n
            read(iu,*) j1, j2, j3, j4
            read(iu,*) (S(k,is),is=1,j4)
         enddo
         do k=1,n
            read(iu,*) j1, j2, j3, j4
            read(iu,*) (Q(k,is),is=1,j4)
         enddo
         do k=1,n
            read(iu,*) (P0(k,j),j=1,4)
         enddo
         close(iu)

         call SETALL(iseed,iseed)
         do k=1,n
            ym(k) = 0
         enddo
         call SETGMN (ym,Q,n,parm)

         do k=1,n
            w_ns = X_ns(k)
            w_k = X_k(k)
            do j=1,nt
               w_t(j) = X_t(j,k)
               w_c1(j) = X_c1(j,k)
            enddo
            call SPLEV (w_t,w_ns,w_c1,w_k,xm(k),p(k),1,ier)
            wu2(k) = YFCT(x(k),p(k),k)
            wm(k) = 0
         enddo
         iu = 1
         open(iu,file='c2w.err',form='formatted')
         log = 1

      else
         do k=1,n
            wu2(k) = wu1(k)
            wm(k) = 0
         enddo
      endif

      call GENMN (parm,e,wu)
      call SGEMV ('N',n,n,1.,S,n,wu2,1,1.,e,1)
      do j=1,n
         wu2(j) = e(j)
         wn(j) = wu2(j)
         W(j,it) = wn(j) + a(j)
         W(j,it) = (1-ind(j)) * XFCT(W(j,it),p(j),j)
         W(j,it) = W(j,it) + ind(j)*x(j)
         wm(j) = wm(j) + W(j,it)
      enddo

      a1 = -4
      a2 = 4

      do k=1,n
         w_ns = X_ns(k)
         w_k = X_k(k)
         do j=1,nt
            w_t(j) = X_t(j,k)
            w_c1(j) = X_c1(j,k)
         enddo
         call SPLEV (w_t,w_ns,w_c1,w_k,xm(k),p(k),1,ier)
         if (ier .ne. 0) then
            liu = 1
            write(iu,*) 'warning from C2W1 (SPLEV): ier = ', ier
            ind(k) = 1
            a(k) = 0
         endif
         nx = X_nx(k)
         do j=1,nx
            tx(j) = X_tx(j,k)
         enddo
         ny = X_ny(k)
         do j=1,ny
            ty(j) = X_ty(j,k)
         enddo
         nc = X_nc(k)
         do j=1,nc
            c2(j) = X_c2(j,k)
         enddo
         ind(k) = 0
         wu(1) = p(k)
         wu(2) = x(k)

         tolr = 1E-7
         a(k) = ROOT (a1,a2,tolr,PA_M,wu,ier)

         if (ier .eq. 1) then
            liu = 1
            write(iu,*) 'warning from C2W1 (ROOT): (xm,x)(k) = ', k,
     *           xm(k),x(k)
            write(iu,*) wu
            write(iu,*) PA_M(a1,wu), PA_M(a2,wu)
            ind(k) = 1
            a(k) = 0
         endif
      enddo

      do it=2,m
         call GENMN (parm,e,wu)
         call SGEMV ('N',n,n,1.,S,n,wu2,1,1.,e,1)
         do j=1,n
            wu2(j) = e(j)
            wn(j) = wu2(j)
            W(j,it) = wn(j) + a(j)
            W(j,it) = (1-ind(j)) * XFCT(W(j,it),p(j),j)
            W(j,it) = W(j,it) + ind(j)*x(j)
            wm(j) = wm(j) + W(j,it)
         enddo
      enddo

      eps = 0
      qq = 0
      do j=1,n
         wm(j) = wm(j)/m
         eps = eps + (wm(j)-x(j))**2
         qq = qq + x(j)**2
      enddo
      eps = SQRT(eps/qq)

      if (tol .gt. 0 .and. eps .gt. tol) goto 1000

      do j=1,n
         wu1(j) = wn(j)
      enddo

      return
      end
