      logical function is_leap(y)
      integer y
      
      is_leap = (mod(y,4)==0 .and. .not. mod(y,100)==0) .or.
     C(mod(y,400)==0)

      return
      end
